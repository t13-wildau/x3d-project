//naroQuestCounter == 1: Naro wurde noch nicht angesprochen.
//naroQuestCounter == 2: Naro wurde angesprochen aber das Item wurde noch nicht geholt.
//naroQuestCounter == 3: Lederhändlerin wurde angesprochen und das Item aufgenommen.
//naroQuestCounter == 4: Item wurde bei Naro abgegeben.
var naroQuestCounter = 1;

//tondamurQuestCounter  == 1: Tondamur wurde noch nicht angesprochen.
//tondamurQuestCounter  == 2: Tondamur wurde angesprochen aber das Item wurde noch nicht gefunden.
//tondamurQuestCounter  == 3: Das Item wurde in der Kiste hinter dem Bild gefunden aber noch nicht abgegeben.
//tondamurQuestCounter  == 4: Das Item wurde an Tondamur zurückgegeben und die Quest beendet.
var tondamurQuestCounter = 1;

/**
* Diese Funktion stellt den Teil der Rätsellogik (Rätsel: "Der Angstelf") dar, welche seitens
* Naro ausgeführt wird. Hier werden auch die Fälle behandelt, falls der Nutzer den NPC mehrmals
* anklicken sollte. In diesem Fall wird eine kurze Info (Tipp) gegeben, die dem Spieler zur
* Lösung der Aufgabe verhilft.
*/
function naroQuest(value){
  if(value == true){
    if(naroQuestCounter == 1){
      print('Naro sagt: "Hey DU! Gut das du vorbei kommst, denn ich brauche dringend deine Hilfe.\n'+
            'Ich habe mich in Corona verirrt! Kannst du fuer mich einen Schminkkoffer finden,\n'+
            'mit dessen Hilfe ich mich tarnen kann um aus der Stadt zu gelangen?"\n');
      naroQuestCounter++;
    } else if(naroQuestCounter == 2){
      print('Naro sagt: "Der Schminkkoffer muss irgendwo in dem Lederwarenladen sein!"\n');
    } else if(naroQuestCounter == 3){
      print('Naro sagt: "Oh du hast ihn gefunden!! Vielen Dank dass du mir geholfen hast!!!"\n');
      naroQuestCounter++;
    } else {
      print('Naro sagt: "Ich werde deine Hilfe niemals vergessen."\n');
    }
  }
}

/**
* Diese Funktion stellt den Teil der Rätsellogik (Rätsel: "Der Angstelf") dar, welche seitens
* der Haendlerin ausgeführt wird. Hier werden auch die Faelle behandelt, falls der Nutzer den NPC
* mehrmals anklicken sollte. In diesem Fall wird eine kurze Info (Tipp) gegeben, die dem
* Spieler zur Lösung der Aufgabe verhilft.
*/
function receiveNaroQuestItem(value){
  if(value == true){
    if(naroQuestCounter == 2){
      print('Lederhaendlerin sagt: "Guten Tag! Hier hast du deinen Schminkkoffer!" \n');
      naroQuestCounter++;
    } else if(naroQuestCounter == 3){
      print('Lederhaendlerin sagt: "Guten Tag! Du hast den Schminkkoffer doch bereits bekommen!" \n');
    } else {
      print('Lederhaendlerin sagt: "Guten Tag! Wir verkaufen hier Lederwaren. Schaue dich doch erstmal ein bisschen um!" \n');
    }
  }
}

/**
* Diese Funktion stellt den Teil der Rätsellogik (Rätsel: "Tondamur sucht ihren magischen
* Stein Baldomar") dar, welche seitens der Magierin Tondamur ausgeführt wird. Hier werden
* auch die Faelle behandelt, falls der Nutzer den NPC mehrmals anklicken sollte. In diesem
* Fall wird eine kurze Info (Tipp) gegeben, die dem Spieler zur Lösung der Aufgabe verhilft.
*/
function tondamurQuest(value){
  if(value == true){
    if(tondamurQuestCounter == 1){
      print('Tondamur sagt: "Hallo Fremder! Gut das Ihr vorbei kommt, denn ich brauche dringend eure Hilfe.\n'+
            'Mir wurde mein magischer Stein Baldomar entwendet und ich muss ihn wieder haben.\n'+
            'Kannst du das für mich uebernehmen, denn ich kann hier zuzeit nicht weg?"\n');
      tondamurQuestCounter++;
    } else if (tondamurQuestCounter == 2) {
      print('Tondamur sagt: "Ich vermute das Baldomar in irgendeiner Kiste versteckt ist!"\n');
    } else if (tondamurQuestCounter == 3) {
      print('Tondamur sagt: "Ich danke dir vielmals, du warst mir eine sehr grosse Hilfe!"');
      tondamurQuestCounter++;
    } else {
      print('Tondamur sagt: "Ich werde deine Hilfe niemals vergessen."\n');
    }
  }
}

/**
* Diese Funktion stellt den Teil der Rätsellogik (Rätsel: "Tondamur sucht ihren magischen
* Stein Baldomar") dar, welche seitens der Kiste, in der sich der Stein befindet, ausgeführt
* wird. Hier werden auch die Faelle behandelt, falls der Nutzer den NPC mehrmals anklicken
* sollte. In diesem Fall wird eine kurze Info (Tipp) gegeben, die dem Spieler zur Lösung
* der Aufgabe verhilft.
*/
function receiveTondamurQuestItem(value){
  if(value == true){
    if(tondamurQuestCounter < 2){
      print('(In dieser Kiste ist ein Stein, doch du kannst mit ihm nichts anfangen und legst\n ihn wieder zuruck.)');
    } else if(tondamurQuestCounter == 2){
      print('(Bei einem genauen Blick in die Kiste findest du einen seltsamen Stein...\n'+
            '...nach genauerem Hinsehen stellst du fest, dass es der magische Stein Baldomar ist.)');
      tondamurQuestCounter++;
    } else if(tondamurQuestCounter > 2){
      print('(In dieser Kiste ist nichts.)');
    }
  }
}
